Requirements
============

Building the python3 virtualenv should be possible on any platform.

The following system dependencies must be installed:

* Python 3.5+ (download from https://www.python.org/downloads/)
* Microsoft Visual C++ 14.x  (https://wiki.python.org/moin/WindowsCompilers)
* A clone of the Mercurial repository (https://www.mercurial-scm.org/repo/hg)

Building
========

The ``packaging.py`` script automates the process of producing a
virtualenv, using a fixed list of versioned Python packages.

If the script is run without an activated ``Visual C++`` command prompt,
it will search for and use the appropriate compiler.  This allows it to
be run from an MSYS shell.

From the prompt, change to the root of this repository. e.g.
``cd c:\src\thg``.

Next, invoke ``packaging.py`` to produce the virtualenv. You will
need to supply the path to the Python interpreter and the Mercurial
repository to use.::

   $ python3.exe contrib\packaging\packaging.py \
       venv --python "c:\Program Files\Python37\python.exe" \
       --hg "c:\src\hg"

If everything runs as intended, dependencies will be fetched and
installed into the virtualenv in the ``build`` sub-directory,
and Mercurial will be built and installed there from source.
The final line of output should print the name of the generated
virtualenv.

Additional options may be configured. Run
``packaging.py venv --help`` to see a list of program flags.
