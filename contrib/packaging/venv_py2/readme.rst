Requirements
============

Building the python2 virtualenv is intended for running from source on
Windows, or using as an environment for running the installer building
script in the ``winbuild`` repository.  With some minor changes, it should
be possible to build on non Windows platforms too.

The following system dependencies must be installed:

* Python 2.7 (download from https://www.python.org/downloads/)
* Microsoft Visual C++ Compiler for Python 2.7 or Visual Studio 2008
  (https://www.microsoft.com/en-us/download/details.aspx?id=44266)
* Microsoft Visual C++ 14.x  (https://wiki.python.org/moin/WindowsCompilers)

  * MSVC++ 14.0 (VS 2015) can build 32 or 64 bit PyQt5
  * MSVC++ 14.1 (VS 2017) can only build 64 bit PyQt5

* A clone of the Mercurial repository (https://www.mercurial-scm.org/repo/hg)
* Python 3.5+ (to run the ``packaging.py`` script)
* Qt5 (https://download.qt.io/archive/qt/5.9/5.9.9/)

Building
========

The ``packaging.py`` script automates the process of producing a
virtualenv.  It manages fetching and configuring the
non-system dependencies (such as py2exe, gettext, and various
Python packages), and building PyQt5 from source.

If the script is run without an activated ``Visual C++`` command prompt,
it will search for and use the appropriate compiler.  This allows it to
be run from an MSYS shell.  It searches for VC++ 14.1 for 64-bit PyQt5
builds, and VC++14.0 for 32-bit builds.  To use another compiler, simply
run from an activated ``Visual C++`` command prompt
(e.g ``Visual C++ 2015 Command Prompt``).

From the prompt, change to the root of this repository. e.g.
``cd c:\src\thg``.

Next, invoke ``packaging.py`` to produce the virtualenv. You will
need to supply the path to the Python interpreter, the Mercurial
repository to use, and the Qt5 binaries.::

   $ python3.exe contrib\packaging\packaging.py \
       venv_py2 --python "c:\Program Files\Python37\python.exe" \
       --hg "c:\src\hg" \
       --qt "c:\Qt\Qt5.9.9\5.9.9\msvc2017_64\bin"

If everything runs as intended, dependencies will be fetched,
built, and installed into the virtualenv in the ``build``
sub-directory.  Mercurial will be built and installed there from
source.  The final output should print the name of the generated
virtualenv, and some hints for building the installer with the
``winbuild`` repository.

Additional options may be configured. Run
``packaging.py venv_py2 --help`` to see a list of program flags.

A 32-bit virtualenv can be built by passing a 32-bit python
executable as the ``--python`` argument.  Likewise a 64-bit
environment is build by passing a 64-bit python executable.  The
architecture of the Qt binaries must match the python architecture.

Installer
=========

In order to build the installer, an activated ``Visual C++ 2008``
or ``MSVC++ Compiler for Python 2.7`` command prompt is required.
The 64-bit command prompt will build the 64-bit MSI, and requires
running from the 64-bit virtualenv.  The 32-bit command prompt
will build the 32-bit MSI, and requires running from the 32-bit
virtualenv.  (Currently, the ``winbuild`` script tries to build
the 32-bit installer with Qt4, and requires changing a single
conditional to use Qt5 instead.)

Building the virtualenv also downloads some additional dependencies,
and caches them in the ``build`` directory.  The WiX toolset must be
downloaded and installed (https://wixtoolset.org/releases/), and the
new virtualenv must be activated (to put Sphinx in PATH).

Next, add the ``gettext`` program to PATH if it is not already
installed, as indicated by the final message from building the
virtualenv.

Clone https://bitbucket.org/tortoisehg/thg-build, and run ``setup.py``
to collect all of the various extension dependencies.

Change to the root of the ``winbuild`` subdirectory, and run ``setup.py``
to build the MSI installer.::

    $ python setup.py thg-stable --hgtag 5.3

If the build fails because the ``vcxx.crt`` merge modules are missing,
they were downloaded and cached in the ``build/vcredist`` directory when
the virtualenv was created.  Simply copy them to the path indicated in
the failure.

If everything runs as intended, the installer is placed in the ``output``
directory next to the ``winbuild`` directory.
